# Payzos-backend

Payzos is Tezos payment service

# Setup

-   Install Composer packages

```
composer install
```

-   Make .env file

```
cp .env.sample .env
```

-   Fill .env variables

-   Make Vhost and set `public_html` as root (recommended nginx)

## Mysql migration

There is not migration tool for now.\
For setup database just uncomment `// $this->migration();` from `app/Models/Model.php` Line number 30.

and comment it after first usage.

# License

GPL-3
