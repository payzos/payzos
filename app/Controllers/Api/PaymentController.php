<?php

namespace App\Controllers\Api;

use App\Models\Model;
use Lib\Sanitize;
use ConseilPHP\Conseil;
use Lib\Functions;

/**
 * [Description Api]
 */
class PaymentController
{
    /**
     * @var [type]
     */
    private $model;
    /**
     * @var [type]
     */
    private $api;
    /**
     * @var [type]
     */
    private $conseil;
    /**
     * @var [type]
     */
    private $sanitize;

    /**
     * @param mixed $_model
     */
    public function __construct()
    {
        $this->model = new Model();
        $this->sanitize = new Sanitize();
        $this->conseil = new Conseil($_ENV["CONSEIL_API_URL"], $_ENV["CONSEIL_API_PLATFORM"], $_ENV["CONSEIL_API_KEY"], $_ENV["FIXER_API_KEY"]);
    }


    /**
     * @return string(Json)
     */
    public function store()
    {
        $data = json_decode(file_get_contents("php://input"), true);
        if (
            !isset($data["destination_hash"]) ||
            !isset($data["total"]) ||
            !isset($data["currency"]) ||
            !isset($data["back_url"])
        ) {
            return  Functions::error_output(false, "fill values");
        }

        if (!$this->conseil->validate_wallet_hash($data["destination_hash"])) {
            return Functions::error_output("unvalid destination hash");
        }

        $back_url =  filter_var($data["back_url"], FILTER_SANITIZE_URL);
        if (!filter_var($back_url, FILTER_VALIDATE_URL)) {
            return Functions::error_output("unvalid backurl");
        }
        $website = parse_url($back_url, PHP_URL_HOST);
        if (!$website) {
            return Functions::error_output("unvalid backurl");
        }

        if (!$this->sanitize->currency($data["currency"])) {
            return Functions::error_output("unvalid currency");
        }

        $currency = $data["currency"];
        $total = $data["total"];
        if ($total <= 0) {
            return Functions::error_output("unvalid total");
        }
        $xtz_total = $this->conseil->get_xtz_total($total, $currency);
        if (!$xtz_total) {
            return Functions::error_output("can't calculate xtz value");
        }
        $real_amount = ($total . " " . $currency);
        $i = 0;
        while (!$this->model->can_pay_this_amount($xtz_total, $data["destination_hash"])) {
            $i++;
            if ($i > 50) {
                return Functions::error_output("more payment with same value in this time. please try another time.");
            }
            $xtz_total++;
        }
        $payment_start_time = time();

        $payment_id = $this->model->store_payment($back_url, $website, $xtz_total, $real_amount, $data["destination_hash"], $payment_start_time);
        if (!$payment_id || !$this->sanitize->payment_id($payment_id)) {
            return Functions::error_output("sorry i can't make payment");
        }
        return  Functions::data_output([
            "payment_id" =>  $payment_id,
            "url" => $_ENV["PAYZOS_HOST"] . "payment/" . $payment_id
        ]);
    }


    /**
     * @param mixed $_id
     * 
     * @return string(Json)
     */
    public function show(string $_id, bool $_just_proccess = false)
    {
        $id = Functions::validate_payment_id($_id);
        if (!$id) {
            return  Functions::error_output("unvalid ID");
        }

        $this->proccess_payment($id);
        if ($_just_proccess) {
            return true;
        }
        $payment = $this->model->get_payment($id);
        if (!$payment || is_null($payment)) {
            return  Functions::error_output("unvalid payment");
        }
        return  Functions::data_output($payment);
    }


    /**
     * @param string $_id
     * 
     * @return [type]
     */
    private function proccess_payment(string $_id)
    {
        $payment = $this->model->get_payment($_id);
        if (!$payment || is_null($payment)) {
            return false;
        }
        if (!isset($payment["destination_hash"]) || !isset($payment["start_time"]) || !isset($payment["amount"])) {
            return false;
        }
        if ($payment["status"] !== "pending") {
            return false;
        }
        // Conseil
        $conseil_result = $this->conseil->get_payment_detail($payment["destination_hash"],  $payment["amount"], $payment["start_time"]);
        if (!$conseil_result || is_null($conseil_result) || !is_array($conseil_result)) {
            return $this->cancel_payment($payment);
        }
        return $this->finish_payment($payment, $conseil_result);
    }


    /**
     * @param mixed $_payment
     * @param mixed $_conseil
     * 
     * @return [type]
     */
    private function finish_payment($_payment, $_conseil)
    {

        return $this->model->finish_payment($_payment["payment_id"], $_conseil["source"], $_conseil["timestamp"], $_conseil["operation_group_hash"]);
    }


    /**
     * @param mixed $_payment
     * 
     * @return [type]
     */
    private function cancel_payment($_payment)
    {
        if ($_payment["start_time"] >= time() - 900) {
            return false;
        }
        return $this->model->cancel_payment($_payment["payment_id"], time());
    }
}
