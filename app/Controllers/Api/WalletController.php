<?php

namespace App\Controllers\Api;

use Lib\Functions;
use ConseilPHP\Conseil;
use App\Models\Model;
use Lib\Sanitize;

/**
 * [Description WalletController]
 */
class WalletController
{
    /**
     * @var [type]
     */
    private $model;
    /**
     * @var [type]
     */
    private $conseil;
    /**
     * @var [type]
     */
    private $sanitize;

    /**
     */
    public function __construct()
    {
        $this->model = new Model();
        $this->sanitize = new Sanitize();
        $this->conseil = new Conseil($_ENV["CONSEIL_API_URL"], $_ENV["CONSEIL_API_PLATFORM"], $_ENV["CONSEIL_API_KEY"], $_ENV["FIXER_API_KEY"]);
    }

    /**
     * @param string $_id
     * 
     * @return [type]
     */
    public function validation(string $_id)
    {
        return $this->conseil->validate_wallet_hash($_id);
    }


    /**
     * @param string $_id
     * 
     * @return [type]
     */
    public function show(string $_id)
    {
        if (!$this->validation($_id)) {
            return Functions::error_output("unvalid wallet public key");
        }
        $sort_by = (!isset($_GET["sort_by"]))                       ? "id"  : $_GET["sort_by"];
        $status_filter  = (!isset($_GET["status_filter"]))  ? "all"  : $_GET["status_filter"];
        $is_asc  = (isset($_GET["ASC"]) && $_GET["ASC"] == 'true')  ? true  : false;
        if (!($sort_by === "id" || $sort_by === "payment_id" || $sort_by === "transaction_hash" || $sort_by === "amount" || $sort_by === "update_time" || $sort_by === "status" || $sort_by === "website")) {
            return Functions::error_output("unvalid sort_by value");
        }
        if ($status_filter !== "all" && $status_filter !== "approved" && $status_filter !== "pending" && $status_filter !== "canceled") {
            return Functions::error_output("unvalid status filter value");
        }
        $data = $this->model->transaction_list($_id, $sort_by, $is_asc, $status_filter);
        if (!$data) {
            return Functions::error_output("not found any transactions");
        }
        return Functions::data_output($data);
    }

    /**
     * @param string $_id
     * 
     * @return [type]
     */
    public function search(string $_id)
    {
        if (!$this->validation($_id)) {
            return Functions::error_output("unvalid wallet public key");
        }
        $sort_by = (!isset($_GET["sort_by"]))                       ? "id"  : $_GET["sort_by"];
        $status_filter  = (!isset($_GET["status_filter"]))  ? "all"  : $_GET["status_filter"];
        $is_asc  = (isset($_GET["ASC"]) && $_GET["ASC"] == 'true')  ? true  : false;
        if (!($sort_by === "id" || $sort_by === "payment_id" || $sort_by === "transaction_hash" || $sort_by === "amount" || $sort_by === "update_time" || $sort_by === "status" || $sort_by === "website")) {
            return Functions::error_output("unvalid sort_by value");
        }
        if ($status_filter !== "all" && $status_filter !== "approved" && $status_filter !== "pending" && $status_filter !== "canceled") {
            return Functions::error_output("unvalid status filter value");
        }
        if (!isset($_GET["what"])) {
            return Functions::error_output("unvalid input");
        }
        $what = $this->sanitize->search_input($_GET["what"]);
        if (!$what) {
            return Functions::error_output("unvalid data for search");
        }
        $data = $this->model->search_in_transactions($_id, $what, $sort_by, $is_asc, $status_filter);
        if (!$data) {
            return Functions::error_output("we found nothing");
        }
        return Functions::data_output($data);
    }
}
