<?php

namespace App\Controllers;

use App\Controllers\Api\PaymentController;
use App\Controllers\Api\WalletController;
use App\Models\Model;
use Lib\Functions;

class Controller
{

    /**
     * @var [type]
     */
    private $payment_controller;
    /**
     * @var [type]
     */
    private $wallet_controller;
    /**
     * @var [type]
     */
    private $model;


    /**
     */
    public function __construct()
    {
        $this->model = new Model();
        $this->payment_controller = new PaymentController();
        $this->wallet_controller = new WalletController();
    }

    /**
     * 
     * @return array(empty)
     */
    public function home_page()
    {
        return null;
    }


    /**
     * @param mixed $_wallet_hash
     * 
     * require keys with Json Post :
     * [
     *  - destination_hash
     *  - total
     *  - currency
     *  - back_url
     * ]
     * 
     * @return string(JSON)
     */
    public function api_make_payment()
    {
        $this->proccess_expired_payments();
        return $this->payment_controller->store();
    }

    /**
     * @param mixed $_id
     * 
     * @return [type]
     */
    public function api_payment(int $_id)
    {
        return $this->payment_controller->show(strval($_id));
    }

    /**
     * @param mixed $_id
     * 
     * @return [type]
     */
    public function api_wallet_transactions(string $_id)
    {
        $this->proccess_expired_payments();
        return $this->wallet_controller->show($_id);
    }

    /**
     * @param string $_id
     * 
     * @return [type]
     */
    public function api_wallet_transactions_search(string $_id)
    {
        return $this->wallet_controller->search($_id);
    }


    /**
     * @param string $_id
     * 
     * @return [type]
     */
    public function api_wallet_validation(string $_id)
    {
        if ($this->wallet_controller->validation($_id)) {
            return Functions::data_output(true);
        }
        return Functions::error_output("unvalid wallet public key");
    }



    /**
     * 
     * we must to make fail all of expired payment (each payments has 900 second time to pay)
     * 
     * @return void
     */
    private function proccess_expired_payments()
    {
        $payments = $this->model->get_long_pending_payment();
        foreach ($payments as $key => $value) {
            $this->payment_controller->show($value["payment_id"], true);
        }
    }
}
