<?php

namespace App\Models;

use mysqli;

/**
 * [Description Model]
 */
class Model
{
    /**
     * @var string
     */
    private $table_name = 'payments';
    /**
     * @var [type]
     */
    private $db;


    /**
     */
    public function __construct()
    {
        $this->db = new mysqli($_ENV["MYSQL_HOST"], $_ENV["MYSQL_USER_NAME"], $_ENV["MYSQL_PASSWORD"], $_ENV["DATABASE_NAME"], $_ENV["MYSQL_PORT"]);
        if ($this->db->connect_error) {
            die("Connection faild: " . $this->db->connect_error);
        }
        // $this->migration();
    }


    /**
     * @return [type]
     */
    public function migration()
    {
        /**
         * database structure
         * id
         * order_id : store shop order_id
         * back_url : website url
         * amount : xtz amount * 1000000
         * destination_hash : admin tezos hash_id (stored in woocommerce payment configure).
         * origin_hash : after transaction done, we will store which hash id did this transaction.
         * transaction_hash :‌ group block hash (get from conseil)
         * start_time : unixTime.
         * update_time : unixTime.
         * status : - approved - canceled - pending.
         */
        $sql = " CREATE TABLE IF NOT EXISTS " . $this->table_name . " (
            id                  BIGINT(20)   NOT NULL AUTO_INCREMENT,
            payment_id          VARCHAR(20)  NOT NULL UNIQUE,
            back_url            text         NOT NULL,
            website             text         NOT NULL,
            amount              BIGINT(30)   NOT NULL,
            real_amount         TEXT(30)     NOT NULL,
            destination_hash    VARCHAR(200) NOT NULL,
            origin_hash         VARCHAR(200)     NULL,
            transaction_hash    VARCHAR(200)     NULL,
            start_time          BIGINT(20)   NOT NULL,
            update_time         BIGINT(20)       NULL,
            status              VARCHAR(20)  NOT NULL,
            PRIMARY KEY  (id)
        ) DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
        $this->db->query($sql);
    }

    /**
     * store initial payment data
     *
     * @param int       $_order_id
     * @param string    $_back_url
     * @param int       $_amount
     * @param string    $_destination_hash
     * @param int       $_time
     * 
     * @return int      $payment_id
     */
    public function store_payment(string $_back_url, string $_website, int $_amount, string $_real_amount, string $_destination_hash,  int $_time)
    {
        $random_payment_id = \Lib\Functions::generate_payment_id();
        $query = "INSERT INTO " . $this->table_name .
            "(
                back_url, 
                website, 
                payment_id, 
                amount, 
                real_amount, 
                destination_hash, 
                start_time, 
                update_time, 
                status)
             VALUE
              (
                '{$_back_url}',
                '{$_website}',
                '{$random_payment_id}',
                '{$_amount}',
                '{$_real_amount}',
                '{$_destination_hash}',
                {$_time},
                {$_time},
                'pending'
               )";
        $result = $this->db->query($query);
        if (!$result) {
            return false;
        }
        return ("PZ" . $random_payment_id);
    }


    /**
     * update payment after finish
     *
     * @param int       $_payment_id
     * @param string    $_origin_hash
     * @param int       $_time
     * @return boolean
     */
    public function finish_payment($_payment_id, $_origin_hash, $_time, $_transaction_hash)
    {
        if (!is_string($_payment_id) || !is_string($_origin_hash) || !is_int($_time) || !is_string($_transaction_hash)) {
            return false;
        }

        $query = "UPDATE {$this->table_name} 
		SET `origin_hash` = '{$_origin_hash}', `update_time` = '{$_time}', `transaction_hash` = '{$_transaction_hash}', `status` = 'approved'
		WHERE `payment_id` = '{$_payment_id}'";

        $result = $this->db->query($query);
        if (!$result) {
            return false;
        }
        return true;
    }

    /**
     * update payment if canceled
     *
     * @param int $_payment_id
     * @param int $_time
     * @return boolean
     */
    public function cancel_payment($_payment_id, $_time)
    {
        if (!is_string($_payment_id) || !is_int($_time)) {
            return false;
        }
        $query = "UPDATE {$this->table_name} 
		SET  `update_time` = '{$_time}', `status`='canceled'
		WHERE `payment_id` = '{$_payment_id}'";

        $result = $this->db->query($query);

        if (!$result) {
            return false;
        }
        return true;
    }

    /**
     * i must to be sure there is not more than one running payment with a certain amount.
     *
     * @param int $_amount
     * @return boolean
     */
    public function can_pay_this_amount($_amount, $_destination_hash)
    {
        if (!is_numeric($_amount)) {
            return false;
        }
        $result = $this->db->query(
            "SELECT id FROM  {$this->table_name}
              WHERE amount = {$_amount}
              AND status = 'pending' 
              AND destination_hash = '{$_destination_hash}'"
        );
        if (!$result || $result->num_rows < 1) {
            return true;
        }
        return false;
    }

    /**
     * Undocumented function
     *
     * @param [type] $_payment_id
     * @return void
     */
    public function get_payment($_payment_id)
    {
        if (!is_string($_payment_id)) {
            return false;
        }
        $result = $this->db->query(
            "SELECT * FROM " . $this->table_name . " WHERE payment_id = '{$_payment_id}'",
        );
        if (!$result || $result->num_rows < 1) {
            return false;
        }
        while ($payment = $result->fetch_assoc()) {
            return $payment;
        }
    }


    /**
     * @param string $_sort_by
     * @param bool $_ASC
     * 
     * @return [type]
     */
    public function transaction_list(string $_destination_hash, string $_sort_by = "id", bool $_ASC = false, string $_status = "all")
    {
        $status_filter = ($_status === "all") ? null : "AND `status` = '{$_status}'";
        $results = $this->db->query(
            "SELECT  * FROM {$this->table_name}  
              WHERE `destination_hash` = '{$_destination_hash}' {$status_filter}
              ORDER BY `{$_sort_by}` " . (($_ASC) ? "ASC" : "DESC")
        );
        if (!$results || $results->num_rows < 1) {
            return false;
        }
        $data = $results->fetch_all(MYSQLI_ASSOC);
        return $data;
    }


    /**
     * @param string $_what
     * @param string $_filter
     * 
     * @return [type]
     */
    public function search_in_transactions(string $_destination_hash, string $_what, string $_sort_by = "id", bool $_ASC = false, string $_status = "all")
    {
        $status_filter = ($_status === "all") ? null : "AND `status` = '{$_status}'";
        $results = $this->db->query(
            "SELECT * FROM  {$this->table_name}
              WHERE (`payment_id`='{$_what}' OR `transaction_hash`='{$_what}' OR `website`='{$_what}') AND (`destination_hash`='{$_destination_hash}' {$status_filter})
              ORDER BY `{$_sort_by}` " . (($_ASC) ? "ASC" : "DESC")
        );
        if (!$results || $results->num_rows < 1) {
            return [];
        }
        $data = $results->fetch_all(MYSQLI_ASSOC);
        return $data;
    }


    /**
     * get list of payments for run cron job
     *
     * @param integer $_time
     * @return array
     */
    public function get_long_pending_payment($_time = 900)
    {
        if (!is_numeric($_time)) {
            return false;
        }
        $time = time() - $_time;
        $results = $this->db->query(
            "SELECT id, payment_id FROM {$this->table_name}
              WHERE start_time <= {$time} 
              AND status = 'pending'"
        );
        if (!$results || $results->num_rows < 1) {
            return [];
        }
        return $results->fetch_all(MYSQLI_ASSOC);
    }
}
