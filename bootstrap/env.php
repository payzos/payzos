<?php

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/../');
$dotenv->load();
$dotenv->required([
    'HOST',
    'MYSQL_HOST',
    'MYSQL_USER_NAME',
    'MYSQL_PASSWORD',
    'DATABASE_NAME',
    'MYSQL_PORT',
    'CONSEIL_API_URL',
    'CONSEIL_API_KEY',
    'CONSEIL_API_PLATFORM',
    'FIXER_API_KEY',
]);
