# Payzos Back-End Api Documention

## Make payment

-   Route : `/api/make_payment`
-   request type: `POST`
-   input : Json array :

```
- destination_hash *
- total *
- currency *
- back_url *
```

`* all of them are required`

-   output :

```
{
    ok : boolean
    data : {
        payment_id : PAYMENT_ID,
        url : PAYMENT_URL
    }
}
```

### Payment Info

-   Route : `/api/payment/PAYMENT_ID`
-   request type : `GET`
-   output :

```
{
    ok : boolean
    data : {
        id
        payment_id : WITH SPECIAL STRUCTURE PZ{16number}
        back_url :
        website : PAYMENT REFERENCE WEBSITE (maded by back_url)
        amount : XTZ VALUE MUST TO PAID
        real_amount : VALUE CURRENCY (like : 1 USD)
        destination_hash : TEZOS_PUBLIC_WALLET_HASH
        origin_hash : null || TEZOS_PUBLIC_WALLET_HASH
        transaction_hash : null || TRANSACTION_HASH
        start_time : UNIX_TIME
        update_time : UNIX_TIME
        status : pending || canceled ||  approved
    }
}
```

### wallet page (transaction list by destination_hash)

-   Rout : `/api/wallet/WALLET_HASH`
-   request type : `GET`
-   query input parameter :

```
 - sort_by : id || payment_id || transaction_hash || amount || update_time || status || website
 - status_filter : all || approved || pending || cancled
 - ASC : boolean
```

`* none of them are not required`

-   output :

```
[
    ok : boolean,
    data : [
        {
            id
            payment_id
            back_url
            website
            amount
            real_amount
            destination_hash
            origin_hash
            transaction_hash
            start_time
            update_time
            status
        }
    ]
]
```

### wallet page search (search in transactions by destination_hash)

-   Route : `/api/wallet/search/WALLET_HASH`
-   request type : `GET`
-   query input parameter :

```
- what : value of what you want to search
 - sort_by : id || payment_id || transaction_hash || amount || update_time || status || website
 - status_filter : all || approved || pending || cancled
 - ASC : boolean
```

-   output :

```
[
    ok : boolean,
    data : [
        {
            id
            payment_id
            back_url
            website
            amount
            real_amount
            destination_hash
            origin_hash
            transaction_hash
            start_time
            update_time
            status
        }
    ]
]
```

## how back_url work ? (backurl handled in payzos-frontend)

after payment status got update back url will be called.

we will send a `POST` request to back_url with this data :

-   input :

```
{
    payment_id : PAYMENT_ID
}
```

and you must give us this output.

-   output :

```
{
    redirect_url : URL
}
```

we use this redirect_url to redirect user after payment got done.
