<?php

namespace Lib;

class Functions
{

    /**
     * @return [type]
     */
    public static function generate_payment_id()
    {
        $random_payment_id = rand(100000, 999999) . rand(10000, 99999) . rand(10000, 99999);
        if (strlen($random_payment_id) !== 16) {
            return false;
        }
        return $random_payment_id;
    }

    /**
     * @param mixed $_message
     * 
     * @return [type]
     */
    public static function error_output($_message)
    {
        $output = [];
        $output["ok"] = false;
        $output["message"] = $_message;
        return json_encode($output);
    }


    /**
     * @param mixed $_data
     * 
     * @return [type]
     */
    public static function data_output($_data)
    {
        $output = [];
        $output["ok"] = true;
        $output["data"] = $_data;
        return json_encode($output);
    }


    public static function validate_payment_id($_string)
    {
        if (strlen($_string) !== 16) {
            return false;
        }
        return $_string;
    }
}
