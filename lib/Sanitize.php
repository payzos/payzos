<?php

namespace Lib;

class Sanitize
{
    public function currency(string $_currency)
    {
        if (strlen($_currency) > 3) {
            return false;
        }
        return true;
    }

    public function payment_id(string $_payment_id)
    {
        if ($_payment_id[0] === "P" && $_payment_id[1] === "Z") {
            $_payment_id = substr($_payment_id, 2);
        }
        if (strlen($_payment_id) !== 16) {
            return false;
        }
        if (intval($_payment_id) == 0) {
            return false;
        }
        return true;
    }

    public function search_input($_what)
    {
        $what =  filter_var($_what, FILTER_SANITIZE_STRING);
        if ($what[0] === 'P' && $what[1] == "Z") {
            $what = substr($what, 2);
        }
        return $what;
    }
}
