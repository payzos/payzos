<?php

require '../vendor/autoload.php';

$app = require_once __DIR__ . '/../bootstrap/app.php';

if (is_null($app)) {
    include 'page.html';
} else {

    header('Content-type: application/json');
    header('Access-Control-Allow-Origin: *');

    echo $app;
}
