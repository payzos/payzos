<?php

namespace Route;

use App\Controllers\Controller;
use FastRoute;

class Route
{
    private $dispatcher;
    private $controller;
    public function __construct()
    {
        $this->controller = new Controller();
    }


    public function start()
    {
        $this->dispatcher = FastRoute\simpleDispatcher([&$this, 'init']);
        $route_info = $this->dispatch();
        return $this->handle($route_info);
    }

    public function init(FastRoute\RouteCollector $r)
    {
        $r->addRoute('GET', '/', [&$this->controller, 'home_page']);


        $r->addRoute('POST', '/api/make_payment', [&$this->controller, 'api_make_payment']);
        $r->addRoute('GET', '/api/payment/PZ{id:\d+}', [&$this->controller, 'api_payment']);
        $r->addRoute('GET', '/api/wallet/{wallet_hash}', [&$this->controller, 'api_wallet_transactions']);
        $r->addRoute('GET', '/api/wallet/search/{wallet_hash}', [&$this->controller, 'api_wallet_transactions_search']);
        $r->addRoute('GET', '/api/wallet/validation/{wallet_hash}', [&$this->controller, 'api_wallet_validation']);
    }


    public function dispatch()
    {
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = $_SERVER['REQUEST_URI'];

        // Strip query string (?foo=bar) and decode URI
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);

        $route_info = $this->dispatcher->dispatch($httpMethod, $uri);
        return $route_info;
    }


    public function handle($route_info)
    {
        switch ($route_info[0]) {
            case FastRoute\Dispatcher::NOT_FOUND:
                return null;
                break;
            case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                // $allowedMethods = $route_info[1];
                return null;
                break;
            case FastRoute\Dispatcher::FOUND:
                $handler = $route_info[1];
                $vars = $route_info[2];
                $parameter = isset($vars[key($vars)]) ? $vars[key($vars)]  : null;
                return $handler($parameter);
                break;
        }
    }
}
